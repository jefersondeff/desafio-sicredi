# Desafio sicredi

### Requisitos:

- [Python 3.10](https://www.python.org/)
- [Docker](https://docs.docker.com/engine/install/)
- [Docker-compose](https://docs.docker.com/compose/)

## Executando API

```bash
git@gitlab.com:jefersondeff/desafio-sicredi.git
cd desafio-sicredi

docker-compose up -d

```

- http://localhost:8000/api/

## Executando testes unitários:

```bash
cd app
pytest -lv
```

## Executando testes performance:

- [locust documentação](https://locust.io/)

```bash
docker-compose up -d
cd app
locust -f src/test/performance/locust_test.py
```

## Documentação API e Testes

- http://localhost:8000/api/docs
