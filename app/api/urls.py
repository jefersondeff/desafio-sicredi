from contracts.api import router as contract_router
from django.urls import path
from ninja import NinjaAPI
from orders.api import router as orders_router

api = NinjaAPI()
api.add_router("/get_top_n_open_contracts/", contract_router)
api.add_router("/combine_orders/", orders_router)

urlpatterns = [
    path("api/", api.urls),
]
