from unittest import TestCase
from app.src.entities.contract import Contract
from app.src.service.contracts import ContractsService


class TestContract(TestCase):
    def setUp(self) -> None:
        self.service = ContractsService()

    def test_get_top_n_open_contracts_with_renegotiated(self):
        contracts = [
            Contract(id=1, debt=1),
            Contract(id=2, debt=2),
            Contract(id=3, debt=3),
            Contract(id=4, debt=4),
            Contract(id=5, debt=5),
        ]
        renegotiated = [3]
        top_n = 3

        expected = [5, 4, 2]
        result, message = self.service.get_top_n_open_contracts(
            contracts, renegotiated, top_n
        )

        self.assertEqual(expected, result)
        self.assertEqual("", message)

    def test_get_top_n_open_contracts_without_renegotiated(self):
        contracts = [
            Contract(id=1, debt=1),
            Contract(id=2, debt=2),
            Contract(id=3, debt=3),
            Contract(id=4, debt=4),
            Contract(id=5, debt=5),
        ]
        renegotiated = []
        top_n = 3

        expected = [5, 4, 3]
        result, message = self.service.get_top_n_open_contracts(
            contracts, renegotiated, top_n
        )

        self.assertEqual(expected, result)
        self.assertEqual("", message)

    def test_get_top_n_open_contracts_without_contracts(self):
        contracts = []
        renegotiated = [3]
        top_n = 3

        result, message = self.service.get_top_n_open_contracts(
            contracts, renegotiated, top_n
        )

        self.assertEqual([], result)
        self.assertEqual("open_contracts cannot be empty. \n", message)

    def test_get_top_n_open_contracts_with_invalid_top_n(self):
        contracts = [
            Contract(id=1, debt=1),
            Contract(id=2, debt=2),
            Contract(id=3, debt=3),
            Contract(id=4, debt=4),
            Contract(id=5, debt=5),
        ]
        renegotiated = [3]
        top_n = 0

        result, message = self.service.get_top_n_open_contracts(
            contracts, renegotiated, top_n
        )

        self.assertEqual([], result)
        self.assertEqual("top_n must be greater than 0. \n", message)

    def test_get_top_n_open_contracts_without_contracts_with_invalid_top_n(self):
        contracts = []
        renegotiated = [3]
        top_n = 0

        result, message = self.service.get_top_n_open_contracts(
            contracts, renegotiated, top_n
        )

        self.assertEqual([], result)
        self.assertEqual(
            "open_contracts cannot be empty. \ntop_n must be greater than 0. \n",
            message,
        )
