from unittest import TestCase

from app.src.service.ordes import OrdersService


class TestOrder(TestCase):
    def setUp(self):
        self.service = OrdersService()

    def test_combine_orders_expected_orders_two(self):
        requests = [70, 30, 10]
        n_max = 100

        expected_orders = 2

        result, message = self.service.combine_orders(requests, n_max)

        self.assertEqual(expected_orders, result)
        self.assertEqual("", message)

    def test_combine_orders_expected_orders_three(self):
        requests = [10, 30, 10, 10, 10, 20]
        n_max = 100

        expected_orders = 3

        result, message = self.service.combine_orders(requests, n_max)

        self.assertEqual(expected_orders, result)
        self.assertEqual("", message)

    def test_combine_orders_not_requests(self):
        requests = []
        n_max = 100

        expected_orders = 0

        result, message = self.service.combine_orders(requests, n_max)

        self.assertEqual(expected_orders, result)
        self.assertEqual("requests cannot be empty. \n", message)

    def test_combine_orders_not_n_max(self):
        requests = [10, 20]
        n_max = 0

        expected_orders = 0

        result, message = self.service.combine_orders(requests, n_max)

        self.assertEqual(expected_orders, result)
        self.assertEqual("n_max must be greater than 0. \n", message)

    def test_combine_orders_not_requests_and_n_max(self):
        requests = []
        n_max = 0

        expected_orders = 0

        result, message = self.service.combine_orders(requests, n_max)

        self.assertEqual(expected_orders, result)
        self.assertEqual(
            "requests cannot be empty. \nn_max must be greater than 0. \n", message
        )
