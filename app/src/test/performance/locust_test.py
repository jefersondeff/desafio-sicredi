from locust import HttpUser, task, between


class Performance(HttpUser):
    wait_time = between(1, 5)

    @task
    def combine_orders(self):
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        self.client.post(
            url="/api/combine_orders/",
            data='{"requests": [70, 30, 10], "n_max": 100}',
            headers=headers,
        )

    @task
    def get_top_n_open_contracts(self):
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        self.client.post(
            url="/api/get_top_n_open_contracts/",
            json={
                "open_contracts": [
                    {"id": 1, "debt": 1},
                    {"id": 2, "debt": 2},
                    {"id": 3, "debt": 3},
                    {"id": 4, "debt": 4},
                ],
                "renegotiated_contracts": [1],
                "top_n": 3,
            },
            headers=headers,
        )
