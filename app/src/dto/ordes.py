from pydantic import BaseModel


class CombineOrder(BaseModel):
    requests: list[int]
    n_max: int
