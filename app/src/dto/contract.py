from pydantic import BaseModel


class Contract(BaseModel):
    id: int
    debt: float


class Contracts(BaseModel):
    open_contracts: list[Contract]
    renegotiated_contracts: list[int]
    top_n: int
