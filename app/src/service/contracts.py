from operator import attrgetter

from src.entities.contract import Contract


class ContractsService:
    def get_top_n_open_contracts(
        self,
        open_contracts: list[Contract],
        renegotiated_contracts: list[int],
        top_n: int,
    ) -> tuple[list[int], str]:

        if message := self._validate_input(open_contracts, top_n):
            return [], message

        return [
            contract.id
            for contract in sorted(open_contracts, key=attrgetter("debt"), reverse=True)
            if contract.id not in renegotiated_contracts
        ][:top_n], ""

    def _validate_input(self, open_contracts: list[Contract], top_n: int) -> str:
        message = ""
        if not open_contracts:
            message += "open_contracts cannot be empty. \n"

        if top_n <= 0:
            message += "top_n must be greater than 0. \n"

        return message
