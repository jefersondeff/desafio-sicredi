class OrdersService:
    def combine_orders(self, requests: list[int], n_max: int) -> tuple[int, str]:
        if message := self._validate_orders(requests, n_max):
            return 0, message

        requests.sort()
        left, right = 0, len(requests) - 1
        num_trips = 0

        while left <= right:
            if requests[left] + requests[right] <= n_max:
                left += 1
            right -= 1
            num_trips += 1

        return num_trips, ""

    def _validate_orders(self, requests: list[int], n_max: int) -> str:
        message = ""
        if not requests:
            message += "requests cannot be empty. \n"
        if n_max <= 0:
            message += "n_max must be greater than 0. \n"
        return message
