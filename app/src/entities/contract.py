from pydantic import BaseModel


class Contract(BaseModel):
    id: int
    debt: float

    def __str__(self) -> str:
        return f"id={self.id}, debt={self.debt}"
