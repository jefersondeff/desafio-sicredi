from dataclasses import dataclass
from src.service.ordes import OrdersService


@dataclass
class CombineOrdersUseCase:
    ordes: OrdersService = OrdersService()

    @dataclass
    class InputParams:
        requests: list[int]
        n_max: int

    def execute(self, input_params: InputParams) -> tuple[list[int], str]:
        return self.ordes.combine_orders(input_params.requests, input_params.n_max)
