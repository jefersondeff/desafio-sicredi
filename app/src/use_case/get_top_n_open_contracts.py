from dataclasses import dataclass

from pydantic import BaseModel
from src.dto.contract import Contract
from src.service.contracts import ContractsService


@dataclass
class GetTopNOpenContractsUseCase:
    contracts_service: ContractsService = ContractsService()

    class InputParams(BaseModel):
        open_contracts: list[Contract]
        renegotiated_contracts: list[int]
        top_n: int

    def execute(self, input_params: InputParams) -> tuple[list[int], str]:
        return self.contracts_service.get_top_n_open_contracts(
            open_contracts=input_params.open_contracts,
            renegotiated_contracts=input_params.renegotiated_contracts,
            top_n=input_params.top_n,
        )
