from ninja import Router
from ninja.errors import ValidationError
from src.dto.ordes import CombineOrder
from src.use_case.combine_ordes import CombineOrdersUseCase

router = Router()


@router.post("/")
def combine_orders(request, item: CombineOrder):
    result, message = CombineOrdersUseCase().execute(
        CombineOrdersUseCase.InputParams(requests=item.requests, n_max=item.n_max)
    )
    if message:
        raise ValidationError(message)
    return result
