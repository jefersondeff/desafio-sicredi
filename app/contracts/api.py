from ninja import Router
from ninja.errors import ValidationError
from src.dto.contract import Contracts
from src.use_case.get_top_n_open_contracts import GetTopNOpenContractsUseCase

router = Router()


@router.post("/")
def get_top_n_open_contracts(request, item: Contracts):
    result, message = GetTopNOpenContractsUseCase().execute(
        GetTopNOpenContractsUseCase.InputParams(
            open_contracts=item.open_contracts,
            renegotiated_contracts=item.renegotiated_contracts,
            top_n=item.top_n,
        )
    )
    if message:
        raise ValidationError(message)
    return result
